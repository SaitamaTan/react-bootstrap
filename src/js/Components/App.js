import React, { Component } from 'react';
import LinkContainer from './LinkContainer';
import Main from './Main';

import '../../css/style.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <LinkContainer />
        <Main />
      </div>
    );
  }
}

export default App;
